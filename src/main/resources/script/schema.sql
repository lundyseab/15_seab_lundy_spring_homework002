
CREATE TABLE IF NOT EXISTS product_tb(
    product_id serial UNIQUE PRIMARY KEY,
    product_name varchar(255) NOT NULL,
    product_price DOUBLE PRECISION NOT NULL
);

CREATE TABLE IF NOT EXISTS customer_tb(
    customer_id serial UNIQUE PRIMARY KEY,
    customer_name varchar(255) NOT NULL ,
    customer_address varchar(255) ,
    customer_phone varchar(25)
);

CREATE TABLE IF NOT EXISTS invoice_tb(
    invoice_id serial UNIQUE PRIMARY KEY,
    invoice_date timestamp default now(),
    customer_id int NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES customer_tb (customer_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS invoice_detail_tb (
    id serial UNIQUE PRIMARY KEY,
    invoice_id int NOT NULL ,
    product_id int NOT NULL,
    FOREIGN KEY (invoice_id) REFERENCES invoice_tb (invoice_id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (product_id) REFERENCES product_tb (product_id) ON DELETE CASCADE ON UPDATE CASCADE
);