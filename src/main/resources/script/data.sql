--Customer Section

SELECT * FROM customer_tb;

SELECT * FROM customer_tb WHERE customer_id = 4;

INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
VALUES ('Johny Put', 'Phnom Penh', '0234235252'),
       ('Phearun', 'BTB', '023423') RETURNING *;

DELETE FROM customer_tb WHERE customer_id = 5;

UPDATE customer_tb SET customer_name = 'Hello', customer_address = 'PV', customer_phone = '099999999'
WHERE customer_id = 6 RETURNING *;


-- Product Section
SELECT * FROM product_tb;

INSERT INTO product_tb (product_name, product_price)
VALUES ( 'Name', 324),
       ('Cola-Cola', 1.2),
       ('Pepsi', 1.2) RETURNING *;

SELECT * FROM product_tb WHERE product_id = 2;

DELETE FROM product_tb WHERE product_id = 1;

UPDATE product_tb SET product_name = 'update', product_price = 57
WHERE product_id = 3 RETURNING *;