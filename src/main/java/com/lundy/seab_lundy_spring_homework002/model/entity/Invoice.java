package com.lundy.seab_lundy_spring_homework002.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    private Integer invoiceId;
    private LocalDateTime invoiceDate;
    private Integer customerId;
}
