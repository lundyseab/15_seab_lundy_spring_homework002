package com.lundy.seab_lundy_spring_homework002.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer productId;
    private String productName;
    private Double productPrice;
}
