package com.lundy.seab_lundy_spring_homework002.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer customerId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;
}
