package com.lundy.seab_lundy_spring_homework002.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceDetail {
    private Integer id;
    private Integer invoiceId;
    private Integer productId;

}
