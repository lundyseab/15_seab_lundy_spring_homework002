package com.lundy.seab_lundy_spring_homework002.model.response;

import com.lundy.seab_lundy_spring_homework002.model.entity.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EntityResponse<T> {
    private T payload;
    private String message;
    private String success;
}
