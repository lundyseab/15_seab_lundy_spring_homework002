package com.lundy.seab_lundy_spring_homework002.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EntityGetAllResponse<T> {
    private List<T> payload;
    private String message;
    private String success;

}
