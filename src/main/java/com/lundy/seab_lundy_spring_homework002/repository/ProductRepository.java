package com.lundy.seab_lundy_spring_homework002.repository;

import com.lundy.seab_lundy_spring_homework002.model.entity.Product;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {
    @Select("""
            SELECT * FROM product_tb;
            """)
    @Results(id = "productMap", value = {
            @Result(property = "productId" , column = "product_id"),
            @Result(property = "productName" , column = "product_name"),
            @Result(property = "productPrice" , column = "product_price"),
    })
    List<Product> getAllProduct();
    @Select("""
            INSERT INTO product_tb (product_name, product_price)
            VALUES (#{p.productName}, #{p.productPrice}) RETURNING *;
            """)
    @ResultMap("productMap")
    Product insertProduct(@Param("p") Product product);

    @Select("""
            SELECT * FROM product_tb WHERE product_id = #{id};
            """)
    @ResultMap("productMap")
    Product getProductById(Integer id);

    @Delete("""
            DELETE FROM product_tb WHERE product_id = #{id};
            """)
    void deleteProductById(Integer id);

    @Select("""
            UPDATE product_tb SET product_name = #{p.productName}, product_price = #{p.productPrice}
            WHERE product_id = #{id} RETURNING *;
            """)
    @ResultMap("productMap")
    Product updateProductById(Integer id, @Param("p") Product product);
}
