package com.lundy.seab_lundy_spring_homework002.repository;

import com.lundy.seab_lundy_spring_homework002.model.entity.Customer;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CustomerRepository {

    @Select("""
            SELECT * FROM customer_tb;
            """)
    @Results(id = "customerMap", value = {
            @Result(property = "customerId" , column = "customer_id"),
            @Result(property = "customerName" , column = "customer_name"),
            @Result(property = "customerAddress" , column = "customer_address"),
            @Result(property = "customerPhone" , column = "customer_Phone")
    })
    List<Customer> getAllCustomer();
    @Select("""
            INSERT INTO customer_tb (customer_name, customer_address, customer_phone)
            VALUES (#{c.customerName}, #{c.customerAddress},  #{c.customerPhone}) RETURNING *;
            """)
    @ResultMap("customerMap")
    Customer insertCustomer(@Param("c") Customer customer);

    @Select("""
            SELECT * FROM customer_tb WHERE customer_id = #{id};
            """)
    @ResultMap("customerMap")
    Customer getCustomerById(Integer id);

    @Delete("""
            DELETE FROM customer_tb WHERE customer_id = #{id};
            """)
    void deleteCustomerById(Integer id);

    @Select("""
            UPDATE customer_tb SET customer_name = #{c.customerName}, customer_address = #{c.customerAddress}, customer_phone = #{c.customerPhone}
            WHERE customer_id = #{id} RETURNING *;
            """)
    @ResultMap("customerMap")
    Customer updateCustomerById(Integer id, @Param("c") Customer customer);
}
