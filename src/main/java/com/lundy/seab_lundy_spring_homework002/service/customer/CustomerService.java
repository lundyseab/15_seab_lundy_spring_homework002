package com.lundy.seab_lundy_spring_homework002.service.customer;

import com.lundy.seab_lundy_spring_homework002.model.entity.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> getAllCustomer();
    Customer getCustomerById(Integer id);
    Customer insertCustomer(Customer customer);
    Customer updateCustomerById(Integer id, Customer customer);

    void deleteCustomerById(Integer id);
}
