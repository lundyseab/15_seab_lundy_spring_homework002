package com.lundy.seab_lundy_spring_homework002.service.customer;

import com.lundy.seab_lundy_spring_homework002.model.entity.Customer;
import com.lundy.seab_lundy_spring_homework002.repository.CustomerRepository;
import com.lundy.seab_lundy_spring_homework002.service.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer insertCustomer(Customer customer) {
        return customerRepository.insertCustomer(customer);
    }

    @Override
    public Customer updateCustomerById(Integer id, Customer customer) {
        return customerRepository.updateCustomerById(id, customer);
    }

    @Override
    public void deleteCustomerById(Integer id) {
        customerRepository.deleteCustomerById(id);
    }


}
