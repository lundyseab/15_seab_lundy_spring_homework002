package com.lundy.seab_lundy_spring_homework002.service.product;

import com.lundy.seab_lundy_spring_homework002.model.entity.Product;
import com.lundy.seab_lundy_spring_homework002.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private final  ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product insertCProduct(Product product) {
        return productRepository.insertProduct(product);
    }

    @Override
    public Product updateProductById(Integer id, Product product) {
        return productRepository.updateProductById(id, product);
    }

    @Override
    public void deleteProductById(Integer id) {
        productRepository.deleteProductById(id);
    }
}
