package com.lundy.seab_lundy_spring_homework002.service.product;

import com.lundy.seab_lundy_spring_homework002.model.entity.Product;

import java.util.List;

public interface ProductService {
        List<Product> getAllProduct();
        Product getProductById(Integer id);
        Product insertCProduct(Product product);
        Product updateProductById(Integer id, Product product);

        void deleteProductById(Integer id);
}
