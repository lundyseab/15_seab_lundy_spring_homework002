package com.lundy.seab_lundy_spring_homework002.controller;

import com.lundy.seab_lundy_spring_homework002.model.entity.Customer;
import com.lundy.seab_lundy_spring_homework002.model.response.EntityResponse;
import com.lundy.seab_lundy_spring_homework002.model.response.EntityGetAllResponse;
import com.lundy.seab_lundy_spring_homework002.model.response.DeleteResponse;
import com.lundy.seab_lundy_spring_homework002.service.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    //Add new customer
    @PostMapping("add-new-customer")
    ResponseEntity<?> insertCustomer(@RequestBody Customer customer){
        try {
            return ResponseEntity.ok(new EntityResponse<Customer>(
                    customerService.insertCustomer(customer),
                    "Successfully add new Customer",
                    "true"
            ));
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    //Get customer by ID
    @GetMapping("get-customer-by-id/{id}")
    ResponseEntity<?> getCustomerById(@PathVariable Integer id){
        try {
            Customer customer = customerService.getCustomerById(id);
            if (customer == null){
                return ResponseEntity.notFound().build();
            }else {
                return ResponseEntity.ok(new EntityResponse<>(
                        customer,
                        "Customer founded!",
                        "true"
                ));
            }
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    //Get all customer
    @GetMapping("get-all-customer")
    ResponseEntity<?> getAllCustomer(){
        try{
            List<?> customers = customerService.getAllCustomer();
            return ResponseEntity.ok(new EntityGetAllResponse<>(
                    customers,
                    "You get all customer successfully",
                    "true"
            ));
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    //Delete by Id
    @DeleteMapping("delete-customer-by-id/{id}")
    ResponseEntity<?> deleteCustomerById(@PathVariable Integer id){
        try {
            if (customerService.getCustomerById(id) == null){
                return ResponseEntity.notFound().build();
            }
            customerService.deleteCustomerById(id);
            return ResponseEntity.ok(new DeleteResponse(
                    "You delete customer id = "+id+", successfully!",
                    true
            ));
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    //Update Customer
    @PutMapping("update-customer-by-id/{id}")
    ResponseEntity<?> updateCustomerById(@PathVariable Integer id, @RequestBody Customer customer){
        try {
            Customer c = customerService.updateCustomerById(id, customer);
            if (c == null){
                return ResponseEntity.notFound().build();
            }else {
                return ResponseEntity.ok(new EntityResponse<>(
                        c,
                        "Customer Updated!",
                        "true"
                ));
            }
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

}
