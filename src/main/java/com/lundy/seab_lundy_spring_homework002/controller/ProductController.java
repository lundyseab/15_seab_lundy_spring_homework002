package com.lundy.seab_lundy_spring_homework002.controller;

import com.lundy.seab_lundy_spring_homework002.model.entity.Product;
import com.lundy.seab_lundy_spring_homework002.model.response.EntityGetAllResponse;
import com.lundy.seab_lundy_spring_homework002.model.response.EntityResponse;
import com.lundy.seab_lundy_spring_homework002.model.response.DeleteResponse;
import com.lundy.seab_lundy_spring_homework002.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    //Add new Product
    @PostMapping("add-new-product")
    ResponseEntity<?> insertProduct(@RequestBody Product product){
        try {
            return ResponseEntity.ok(new EntityResponse<>(
                    productService.insertCProduct(product),
                    "Successfully add new Product",
                    "true"
            ));
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    //Get Product by ID
    @GetMapping("get-product-by-id/{id}")
    ResponseEntity<?> getProductById(@PathVariable Integer id){
        try {
            Product product = productService.getProductById(id);
            if (product == null){
                return ResponseEntity.notFound().build();
            }else {
                return ResponseEntity.ok(new EntityResponse<>(
                        product,
                        "Product founded!",
                        "true"
                ));
            }
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    //Get all Product
    @GetMapping("get-all-product")
    ResponseEntity<?> getAllProduct(){
        try{
            List<?> customers = productService.getAllProduct();
            return ResponseEntity.ok(new EntityGetAllResponse<>(
                    customers,
                    "You get all product successfully",
                    "true"
            ));
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    //Delete by Id
    @DeleteMapping("delete-product-by-id/{id}")
    ResponseEntity<?> deleteProductById(@PathVariable Integer id){
        try {
            if (productService.getProductById(id) == null){
                return ResponseEntity.notFound().build();
            }
            productService.deleteProductById(id);
            return ResponseEntity.ok(new DeleteResponse(
                    "You delete product id = "+id+", successfully!",
                    true
            ));
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }

    //Update Product
    @PutMapping("update-product-by-id/{id}")
    ResponseEntity<?> updateProductById(@PathVariable Integer id, @RequestBody Product product){
        try {
            Product p = productService.updateProductById(id, product);
            if (p == null){
                return ResponseEntity.notFound().build();
            }else {
                return ResponseEntity.ok(new EntityResponse<>(
                        p,
                        "Product Updated!",
                        "true"
                ));
            }
        }catch (Exception e){
            return ResponseEntity.notFound().build();
        }
    }




}
